def find_resource_tiles(game_state):
    resource_tiles = []
    width, height = game_state.map.width, game_state.map.height
    for y in range(height):
        for x in range(width):
            cell = game_state.map.get_cell(x, y)
            if cell.has_resource():
                resource_tiles.append(cell)
    return resource_tiles
