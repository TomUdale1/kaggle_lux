def city_actions(player):
    city_actions_list = []

    units_cap = sum([len(x.citytiles) for x in player.cities.values()])
    units = len(player.units)
    cities = list(player.cities.values())

    if len(cities) > 0:
        city = cities[0]
        created_worker = (units >= units_cap)
        for city_tile in city.citytiles[::-1]:
            if city_tile.can_act():
                if created_worker:
                    action = city_tile.research()
                    city_actions_list.append(action)
                else:
                    action = city_tile.build_worker()
                    city_actions_list.append(action)
                    created_worker = True
    return city_actions_list
