import math

from lux.constants import Constants
from lux.game import Game
from src.resource import find_resource_tiles
from src.worker import get_closest_resource_tile
from src.unit import get_closest_city_tile
from src.city import city_actions

DIRECTIONS = Constants.DIRECTIONS
game_state = None


def agent(observation, configuration):
    global game_state

    if observation["step"] == 0:
        game_state = Game()
        game_state._initialize(observation["updates"])
        game_state._update(observation["updates"][2:])
        game_state.id = observation.player
    else:
        game_state._update(observation["updates"])

    actions = []

    player = game_state.players[observation.player]
    opponent = game_state.players[(observation.player + 1) % 2]

    resource_tiles = find_resource_tiles(game_state)

    actions.extend(city_actions(player))


    # we iterate over all our units and do something with them
    for unit in player.units:
        if unit.is_worker() and unit.can_act():
            closest_resource_tile, closest_resource_dist = get_closest_resource_tile(
                unit, resource_tiles, player
            )
            if closest_resource_tile is not None:
                actions.append(
                    unit.move(unit.pos.direction_to(closest_resource_tile.pos))
                )
            else:
                # if unit is a worker and there is no cargo space left, and we have cities, lets return to them
                if len(player.cities) > 0:
                    closest_city_tile, closest_city_dist = get_closest_city_tile(unit, player)
                    if closest_city_tile is not None:
                        move_dir = unit.pos.direction_to(closest_city_tile.pos)
                        actions.append(unit.move(move_dir))

    # you can add debug annotations using the functions in the annotate object
    # actions.append(annotate.circle(0, 0))

    return actions
